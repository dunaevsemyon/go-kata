package algo

func BubbleSort(data []int) []int {
	swapped := true
	for swapped {
		swapped = false
		for i := 1; i < len(data); i++ {
			if data[i-1] > data[i] {
				data[i], data[i-1] = data[i-1], data[i]
				swapped = true
			}
		}
	}
	return data
}
