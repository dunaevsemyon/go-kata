package main

import (
	"fmt"
	"gitlab.com/dunaevsemyon/go-kata/module3/task/patterns/1/discount"
)

func main() {
	order := discount.Order{
		Name:  "Nvidia RTX 4090",
		Price: 134000,
		Count: 5,
	}

	strategies := []discount.PricingStrategy{discount.RegularPricing{},
		discount.SalePricing{Title: "Black Friday Sale", Discount: 0.15},
		discount.SalePricing{Title: "New Year Sale", Discount: 0.10},
		discount.SalePricing{Title: "Promo Sale", Discount: 0.05},
	}

	for _, strategy := range strategies {
		cost, message := strategy.Calculate(order)
		fmt.Printf("%s %.2f\n", message, cost)
	}
}
