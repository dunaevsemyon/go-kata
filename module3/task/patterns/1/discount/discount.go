package discount

import "fmt"

type PricingStrategy interface {
	Calculate(order Order) (float64, string)
}

type RegularPricing struct {
}

type SalePricing struct {
	Title    string
	Discount float64
}

type Order struct {
	Name  string
	Price float64
	Count int
}

func (r RegularPricing) Calculate(order Order) (float64, string) {
	cost := order.Price * float64(order.Count)
	message := fmt.Sprintf("%s - Total cost without discount: ",
		order.Name)
	return cost, message
}

func (s SalePricing) Calculate(order Order) (float64, string) {
	cost := order.Price * float64(order.Count) * (1 - s.Discount)
	message := fmt.Sprintf("%s - Total cost with \"%v\": ", order.Name,
		s.Title)
	return cost, message
}
