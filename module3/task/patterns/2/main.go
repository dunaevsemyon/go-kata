package main

import (
	"fmt"
	"sort"
	"time"
)

// AirConditioner is the interface that defines the methods for controlling the air conditioner
type AirConditioner interface {
	TurnOn()
	TurnOff()
	SetTemperature(temp int)
}

// RealAirConditioner is the implementation of the air conditioner
type RealAirConditioner struct {
	temperature int
	active      bool
}

type AirConditionerAdapter struct {
	airConditioner *RealAirConditioner
}

type AirConditionerProxy struct {
	adapter   *AirConditionerAdapter
	auth      bool
	accessLog map[time.Time]string
}

func NewAirConditionerProxy(auth bool,
	accessLog map[time.Time]string) *AirConditionerProxy {
	return &AirConditionerProxy{
		adapter: &AirConditionerAdapter{
			airConditioner: &RealAirConditioner{
				active: false,
			},
		},
		auth:      auth,
		accessLog: accessLog,
	}
}

func (r *RealAirConditioner) TurnOn() {
	r.active = true
}

func (r *RealAirConditioner) TurnOff() {
	r.active = false
}

func (r *RealAirConditioner) SetTemperature(t int) {
	r.temperature = t
}

func (a *AirConditionerAdapter) TurnOn() {
	a.airConditioner.TurnOn()
}

func (a *AirConditionerAdapter) TurnOff() {
	a.airConditioner.TurnOff()
}

func (a *AirConditionerAdapter) SetTemperature(t int) {
	a.airConditioner.SetTemperature(t)
}

func Journal(p *AirConditionerProxy) {
	keys := make([]time.Time, 0, len(p.accessLog))
	for k := range p.accessLog {
		keys = append(keys, k)
	}

	sort.Slice(keys, func(i, j int) bool {
		return keys[i].Before(keys[j])
	})

	fmt.Println("Access journal:")
	for _, k := range keys {
		fmt.Printf("%v - %v\n", k.Format(time.Stamp), p.accessLog[k])
	}
}

func (p *AirConditionerProxy) TurnOn() {
	if p.auth {
		p.adapter.airConditioner.TurnOn()
		fmt.Println("Turning on the air conditioner")
		p.accessLog[time.Now()] = "Authenticated access: turning on the air" +
			" conditioner"
	} else {
		fmt.Println("Access denied: authentication required to turn on the" +
			" air conditioner")
		p.accessLog[time.Now()] = "Turning on failed: authentication required" +
			" for"
	}
}

func (p *AirConditionerProxy) TurnOff() {
	if p.auth {
		p.adapter.airConditioner.TurnOff()
		fmt.Println("Turning off the air conditioner")
		p.accessLog[time.Now()] = "Authenticated access: turning off the air" +
			" conditioner"
	} else {
		fmt.Println("Access denied: authentication required to turn off the" +
			" air conditioner")
		p.accessLog[time.Now()] = "Turning off failed: authentication" +
			" required"
	}
}
func (p *AirConditionerProxy) SetTemperature(t int) {
	if p.auth {
		p.adapter.airConditioner.SetTemperature(t)
		fmt.Printf("Setting temperature to %d\n", t)
		p.accessLog[time.Now()] = "Authenticated access: setting temperature"
	} else {
		fmt.Println("Access denied: authentication required to set the" +
			" temperature")
		p.accessLog[time.Now()] = "Setting temperature failed: authentication" +
			" required"
	}
}

func main() {
	var accessLog = make(map[time.Time]string)

	airConditioner := NewAirConditionerProxy(false, accessLog) // without auth
	airConditioner.TurnOn()
	airConditioner.TurnOff()
	airConditioner.SetTemperature(25)

	airConditioner = NewAirConditionerProxy(true, accessLog) // with auth
	airConditioner.TurnOn()
	airConditioner.TurnOff()
	airConditioner.SetTemperature(25)

	Journal(airConditioner)
}
