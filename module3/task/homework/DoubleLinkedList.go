package task

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"math/rand"
	"os"
	"time"

	"github.com/brianvoe/gofakeit/v6"
)

const permissions = 0644

type Commit struct {
	Message string    `json:"message"`
	UUID    string    `json:"uuid"`
	Date    time.Time `json:"date"`
}

type Node struct {
	Data *Commit
	prev *Node
	next *Node
}

type DoubleLinkedList struct {
	head *Node // 1st element
	tail *Node // last element
	curr *Node // current element. modified by next and prev
	len  int   // number of elements
}

type LinkedLister interface {
	LoadData(path string) error

	Append(c *Commit) error
	Prepend(c *Commit) error
	Insert(c *Commit) (*Node, error)

	JumpTo(n int) (*Node, error)
	Len() int
	Current() *Node
	Next() *Node
	Prev() *Node

	Pop() *Node
	Shift() Node
	DeleteCurrent() error
	Delete(n int) error
	SearchUUID(uuID string) (*Node, error)
	Search(message string) (*Node, error)
	Reverse(d *DoubleLinkedList) error
	GenerateJson(count int, path string) error
}

// Quicksort sort by date
func QuickSort(commits []*Commit) []*Commit {
	if len(commits) <= 1 {
		return commits
	}

	median := commits[len(commits)/2].Date

	var lowPart, middlePart, highPart []*Commit

	for _, commit := range commits {
		switch {
		case commit.Date.Before(median):
			lowPart = append(lowPart, commit)
		case commit.Date.After(median):
			highPart = append(highPart, commit)
		case commit.Date.Equal(median):
			middlePart = append(middlePart, commit)
		}
	}

	lowPart = QuickSort(lowPart)
	highPart = QuickSort(highPart)

	return append(append(lowPart, middlePart...), highPart...)
}

// ReadCommitsFromJson
func ReadCommitsFromJson(path string) ([]*Commit, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, fmt.Errorf("error reading file")
	}
	defer func() {
		err := f.Close()
		if err != nil {
			log.Println("error closing file")
		}
	}()

	var commits []*Commit
	byteResult, err := io.ReadAll(f)
	if err != nil {
		return nil, err
	}

	if err := json.Unmarshal(byteResult, &commits); err != nil {
		return nil, err
	}

	return commits, nil
}

// LoadData load commits from json file to DoubleLinkedList
func (d *DoubleLinkedList) LoadData(path string) error {
	commits, err := ReadCommitsFromJson(path)
	if err != nil {
		return err
	}

	commits = QuickSort(commits)

	for _, commit := range commits {
		err := d.Append(commit)
		if err != nil {
			return err
		}
	}

	return nil
}

// Append to the end of the list
func (d *DoubleLinkedList) Append(c *Commit) error {
	if c == nil {
		return fmt.Errorf("cannot append nil commit")
	}
	if d.head == nil {
		newNode := &Node{Data: c, prev: nil, next: nil}
		d.head, d.curr, d.tail = newNode, newNode, newNode
	} else {
		d.tail.next = &Node{Data: c, prev: d.tail, next: nil}
		d.tail = d.tail.next
	}

	d.len++
	return nil
}

// Prepend to the beginning of the list
func (d *DoubleLinkedList) Prepend(c *Commit) error {
	if c == nil {
		return fmt.Errorf("cannot prepend nil commit")
	}
	if d.head == nil {
		newNode := &Node{Data: c, prev: nil, next: nil}
		d.head, d.curr, d.tail = newNode, newNode, newNode
	} else {
		newNode := &Node{Data: c, prev: nil, next: d.head}
		d.head.prev = newNode
		d.head = newNode
	}

	d.len++
	return nil
}

// Insert to the n-th position
func (d *DoubleLinkedList) Insert(n int, c *Commit) error {
	if d == nil {
		return fmt.Errorf("cannot insert nil list")
	}
	if c == nil {
		return fmt.Errorf("cannot insert nil commit")
	}
	if n < 0 || n > d.Len() {
		return fmt.Errorf("index out of range")
	}

	curr, err := d.JumpTo(n)
	if err != nil {
		return err
	}

	newNode := &Node{Data: c, prev: curr, next: curr.next}
	curr.next = newNode
	if newNode.next == nil {
		d.tail = newNode
	} else {
		newNode.next.prev = newNode
	}

	d.len++
	return nil
}

// JumpTo n node
func (d *DoubleLinkedList) JumpTo(n int) (*Node, error) {
	if d == nil {
		return nil, fmt.Errorf("cannot jump to nil list")
	}

	if n < 0 || n > d.Len()-1 {
		return nil, fmt.Errorf("index out of range")
	}
	if n <= (d.Len()-1)/2 {
		d.curr = d.head
		for i := 0; i < n; i++ {
			d.curr = d.curr.next
		}
	} else {
		d.curr = d.tail
		for i := d.Len() - 1; i > n; i-- {
			d.curr = d.curr.prev
		}
	}

	return d.curr, nil
}

// Len returns the number of elements
func (d *DoubleLinkedList) Len() int {
	return d.len
}

// Current return current node
func (d *DoubleLinkedList) Current() *Node {
	if d.curr == nil {
		return nil
	}
	if d.curr == d.head {
		return d.curr
	}

	return d.curr
}

// Next return next node. step next.
func (d *DoubleLinkedList) Next() *Node {
	if d.curr == nil || d.head == nil {
		return nil
	}
	if d.curr == d.tail {
		return d.curr
	}

	d.curr = d.curr.next
	return d.curr
}

// Prev return previous node. step prev.
func (d *DoubleLinkedList) Prev() *Node {
	if d.curr == nil || d.head == nil {
		return nil
	}
	if d.curr == d.head {
		return d.curr
	}

	d.curr = d.curr.prev
	return d.curr
}

// Index return index of current node
func (d *DoubleLinkedList) Index() (int, error) {
	if d.curr == nil {
		return -1, fmt.Errorf("current node not in list")
	}

	i := 0
	indexing := d.head
	for indexing != d.curr {
		indexing = indexing.next
		i++
	}

	return i, nil
}

// Pop return and delete tail
func (d *DoubleLinkedList) Pop() *Node {
	if d.tail == nil {
		return nil
	}
	node := d.tail
	if d.head == d.tail {
		d.head, d.curr, d.tail = nil, nil, nil
	} else {
		d.tail = d.tail.prev
		d.tail.next = nil
	}

	d.len--
	return node
}

// Shift return and delete head
func (d *DoubleLinkedList) Shift() *Node {
	if d.head == nil {
		return nil
	}
	node := d.head
	if d.head == d.tail {
		d.head, d.curr, d.tail = nil, nil, nil
	} else {
		d.head = d.head.next
		d.head.prev = nil
	}

	d.len--
	return node
}

// SearchUUID search commit by UUID
func (d *DoubleLinkedList) SearchUUID(uuID string) (*Node, error) {
	if uuID == "" {
		return nil, fmt.Errorf("'uuID' argument is empty")
	}

	head := d.head
	for head != nil {
		if head.Data.UUID == uuID {
			return head, nil
		}
		head = head.next
	}

	return nil, nil
}

// Search search commit by message
func (d *DoubleLinkedList) Search(message string) (*Node, error) {
	if message == "" {
		return nil, fmt.Errorf("'message' argument is empty")
	}

	head := d.head
	for head != nil {
		if head.Data.Message == message {
			return head, nil
		}
		head = head.next
	}

	return nil, nil
}

// DeleteCurrent delete current element
func (d *DoubleLinkedList) DeleteCurrent() error {
	switch {
	case d.curr == nil:
		return fmt.Errorf("current element is empty")
	case d.curr == d.head:
		d.head.next = nil
		d.head = d.curr
		d.head.prev = nil
	case d.curr == d.tail:
		d.tail.prev.next = nil
		d.tail = d.tail.prev
		d.curr = d.tail
	default:
		d.curr.prev.next = d.curr.next
		d.curr.next.prev = d.curr.prev
		d.curr = d.curr.prev
	}

	d.len--
	return nil
}

// Delete n element
func (d *DoubleLinkedList) Delete(n int) error {
	_, err := d.JumpTo(n)
	if err != nil {
		return err
	}

	err = d.DeleteCurrent()
	if err != nil {
		return err
	}

	return err
}

// Reverse return reversed list
func (d *DoubleLinkedList) Reverse() *DoubleLinkedList {
	if d.head == nil || d.head.next == nil {
		return d
	}

	curr := d.head
	for curr != nil {
		curr.prev, curr.next = curr.next, curr.prev
		curr = curr.prev
	}

	d.head, d.tail = d.tail, d.head
	return d
}

// GenerateJson create and save json file with 'count' number of fake Commits
func GenerateJson(count int, path string) error {
	var commits []Commit
	for i := 0; i < count; i++ {
		commits = append(commits, Commit{
			Message: gofakeit.LoremIpsumSentence(6),
			UUID:    gofakeit.UUID(),
			Date:    time.Now().Add(time.Duration(rand.Intn(300)) * time.Second),
		})
	}

	jsonString, err := json.Marshal(commits)
	if err != nil {
		return err
	}

	err = os.WriteFile(path, jsonString, permissions)
	if err != nil {
		return err
	}

	return nil
}
