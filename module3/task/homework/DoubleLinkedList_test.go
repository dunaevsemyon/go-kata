package task

import (
	"reflect"
	"testing"
	"time"
)

const count = 10 // number of commits to genereate

func TestDoubleLinkedList_Append(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}

	var head = &Node{
		Data: &Commit{
			Message: "Head",
			UUID:    "1",
			Date:    time.Now(),
		},
		prev: nil,
	}

	type args struct {
		c *Commit
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		len     int
		wantErr bool
	}{
		{
			name: "Append at empty list",
			fields: fields{
				head: nil,
				curr: nil,
				tail: nil,
				len:  0,
			},
			args: args{
				c: &Commit{
					Message: "1",
					UUID:    "1",
					Date:    time.Now(),
				},
			},
			len:     1,
			wantErr: false,
		},
		{
			name: "Append",
			fields: fields{
				head: head,
				curr: head,
				tail: head,
				len:  1,
			},
			args: args{
				c: &Commit{
					Message: "2",
					UUID:    "2",
					Date:    time.Now(),
				},
			},
			len:     2,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if err := d.Append(tt.args.c); (err != nil) != tt.wantErr {
				t.Errorf("Append() error = %v, wantErr %v", err, tt.wantErr)
			}
			if got := d.Len(); got != tt.len {
				t.Errorf("Len() = %v, want %v", got, tt.len)
			}
		})
	}
}

func TestDoubleLinkedList_Current(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	var head = &Node{
		Data: &Commit{
			Message: "Head",
			UUID:    "1",
			Date:    time.Now(),
		},
		prev: nil,
	}
	var tail = &Node{
		Data: &Commit{
			Message: "Tail",
			UUID:    "3",
			Date:    time.Now(),
		},
		prev: head,
		next: nil,
	}
	head.next = tail

	tests := []struct {
		name   string
		fields fields
		want   *Node
	}{
		{
			name: "Current head",
			fields: fields{
				head: head,
				curr: head,
				tail: tail,
				len:  2,
			},
			want: head,
		},
		{
			name: "Current tail",
			fields: fields{
				head: head,
				curr: tail,
				tail: tail,
				len:  2,
			},
			want: tail,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Current(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Current() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Delete(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	var head = &Node{
		Data: &Commit{
			Message: "Head",
			UUID:    "1",
			Date:    time.Now(),
		},
		prev: nil,
		next: nil,
	}

	type args struct {
		n int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		len     int
		wantErr bool
	}{
		{
			name: "Delete at empty list",
			fields: fields{
				head: nil,
				curr: nil,
				tail: nil,
				len:  0,
			},
			args: args{
				n: 0,
			},
			len:     0,
			wantErr: true,
		},
		{
			name: "Delete",
			fields: fields{
				head: head,
				curr: head,
				tail: head,
				len:  1,
			},
			args: args{
				n: 0,
			},
			len:     0,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if err := d.Delete(tt.args.n); (err != nil) != tt.wantErr {
				t.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
			if got := d.Len(); got != tt.len {
				t.Errorf("Len() = %v, want %v", got, tt.len)
			}
		})
	}
}

func TestDoubleLinkedList_DeleteCurrent(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	var head = &Node{
		Data: &Commit{
			Message: "Head",
			UUID:    "1",
			Date:    time.Now(),
		},
		prev: nil,
	}
	var tail = &Node{
		Data: &Commit{
			Message: "Tail",
			UUID:    "2",
			Date:    time.Now(),
		},
		prev: head,
		next: nil,
	}
	head.next = tail

	tests := []struct {
		name    string
		fields  fields
		len     int
		wantErr bool
	}{
		{
			name: "Delete current at empty list",
			fields: fields{
				head: nil,
				curr: nil,
				tail: nil,
				len:  0,
			},
			len:     0,
			wantErr: true,
		},
		{
			name: "Delete current tail",
			fields: fields{
				head: head,
				curr: tail,
				tail: tail,
				len:  2,
			},
			len:     1,
			wantErr: false,
		},
		{
			name: "Delete current head",
			fields: fields{
				head: head,
				curr: head,
				tail: tail,
				len:  2,
			},
			len:     1,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if err := d.DeleteCurrent(); (err != nil) != tt.wantErr {
				t.Errorf("DeleteCurrent() error = %v, wantErr %v", err, tt.wantErr)
			}
			if got := d.Len(); got != tt.len {
				t.Errorf("Len() = %v, want %v", got, tt.len)
			}

		})
	}
}

func TestDoubleLinkedList_Index(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}

	var head = &Node{
		Data: &Commit{
			Message: "Head",
			UUID:    "1",
			Date:    time.Now(),
		},
		prev: nil,
	}
	var middle = &Node{
		Data: &Commit{
			Message: "Head",
			UUID:    "2",
			Date:    time.Now(),
		},
		prev: head,
	}
	var tail = &Node{
		Data: &Commit{
			Message: "Head",
			UUID:    "3",
			Date:    time.Now(),
		},
		prev: middle,
		next: nil,
	}
	head.next = middle
	middle.next = tail

	tests := []struct {
		name    string
		fields  fields
		want    int
		wantErr bool
	}{
		{
			name: "Index at empty list",
			fields: fields{
				head: nil,
				curr: nil,
				tail: nil,
				len:  0,
			},
			want:    -1,
			wantErr: true,
		},
		{
			name: "Index middle",
			fields: fields{
				head: head,
				curr: middle,
				tail: tail,
				len:  2,
			},
			want:    1,
			wantErr: false,
		},
		{
			name: "Index tail",
			fields: fields{
				head: head,
				curr: tail,
				tail: tail,
				len:  2,
			},
			want:    2,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			got, err := d.Index()
			if (err != nil) != tt.wantErr {
				t.Errorf("Index() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Index() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Insert(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}

	var head = &Node{
		Data: &Commit{
			Message: "Head",
			UUID:    "1",
			Date:    time.Now(),
		},
		prev: nil,
	}

	var tail = &Node{
		Data: &Commit{
			Message: "Head",
			UUID:    "3",
			Date:    time.Now(),
		},
		prev: head,
		next: nil,
	}
	head.next = tail

	type args struct {
		n int
		c *Commit
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		len     int
		wantErr bool
	}{
		{
			name: "Insert at empty list",
			fields: fields{
				head: nil,
				tail: nil,
				curr: nil,
				len:  0,
			},
			args: args{
				n: 1,
				c: &Commit{
					Message: "Middle",
					UUID:    "2",
					Date:    time.Now(),
				},
			},
			len:     0,
			wantErr: true,
		},
		{
			name: "Insert after head",
			fields: fields{
				head: head,
				tail: tail,
				curr: head,
				len:  2,
			},
			args: args{
				n: 0,
				c: &Commit{
					Message: "Middle",
					UUID:    "2",
					Date:    time.Now(),
				},
			},
			len:     3,
			wantErr: false,
		},
		{
			name: "Insert after tail",
			fields: fields{
				head: head,
				tail: tail,
				curr: head,
				len:  2,
			},
			args: args{
				n: 1,
				c: &Commit{
					Message: "Commit",
					UUID:    "2",
					Date:    time.Now(),
				},
			},
			len:     3,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if err := d.Insert(tt.args.n, tt.args.c); (err != nil) != tt.wantErr {
				t.Errorf("Insert() error = %v, wantErr %v", err, tt.wantErr)
			}
			if got := d.Len(); got != tt.len {
				t.Errorf("Len() = %v, want %v", got, tt.len)
			}

		})
	}
}

func TestDoubleLinkedList_JumpTo(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}

	var head = &Node{
		Data: &Commit{
			Message: "Head",
			UUID:    "1",
			Date:    time.Now(),
		},
		prev: nil,
	}

	var tail = &Node{
		Data: &Commit{
			Message: "Head",
			UUID:    "3",
			Date:    time.Now(),
		},
		prev: head,
		next: nil,
	}
	head.next = tail

	type args struct {
		n int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *Node
		wantErr bool
	}{
		{
			name: "JumpTo at empty list",
			fields: fields{
				head: nil,
				tail: nil,
				curr: nil,
				len:  0,
			},
			args: args{
				n: 1,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "JumpTo 0",
			fields: fields{
				head: head,
				tail: tail,
				curr: head,
				len:  2,
			},
			args: args{
				n: 0,
			},
			want:    head,
			wantErr: false,
		},
		{
			name: "JumpTo 1",
			fields: fields{
				head: head,
				tail: tail,
				curr: head,
				len:  2,
			},
			args: args{
				n: 1,
			},
			want:    tail,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			got, err := d.JumpTo(tt.args.n)
			if (err != nil) != tt.wantErr {
				t.Errorf("JumpTo() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("JumpTo() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Len(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}

	var head = &Node{
		Data: &Commit{
			Message: "Head",
			UUID:    "1",
			Date:    time.Now(),
		},
		prev: nil,
	}

	var tail = &Node{
		Data: &Commit{
			Message: "Head",
			UUID:    "3",
			Date:    time.Now(),
		},
		prev: head,
		next: nil,
	}
	head.next = tail

	tests := []struct {
		name   string
		fields fields
		want   int
	}{
		{
			name: "Len at empty list",
			fields: fields{
				head: nil,
				tail: nil,
				curr: nil,
				len:  0,
			},
			want: 0,
		},
		{
			name: "Len 2",
			fields: fields{
				head: head,
				tail: tail,
				curr: head,
				len:  2,
			},
			want: 2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Len(); got != tt.want {
				t.Errorf("Len() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_LoadData(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	type args struct {
		path string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		len     int
		wantErr bool
	}{
		{
			name: "Load to list(100 commits)",
			fields: fields{
				head: nil,
				tail: nil,
				curr: nil,
				len:  0,
			},
			args: args{
				path: "./test.json",
			},
			len:     100,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if err := d.LoadData(tt.args.path); (err != nil) != tt.wantErr {
				t.Errorf("LoadData() error = %v, wantErr %v", err, tt.wantErr)
			}
			if got := d.Len(); got != tt.len {
				t.Errorf("Len() = %v, want %v", got, tt.len)
			}

		})
	}
}

func TestDoubleLinkedList_Next(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}

	var head = &Node{
		Data: &Commit{
			Message: "Head",
			UUID:    "1",
			Date:    time.Now(),
		},
		prev: nil,
	}

	var tail = &Node{
		Data: &Commit{
			Message: "Head",
			UUID:    "3",
			Date:    time.Now(),
		},
		prev: head,
		next: nil,
	}
	head.next = tail

	tests := []struct {
		name   string
		fields fields
		want   *Node
	}{
		{
			name: "Next at empty list",
			fields: fields{
				head: nil,
				tail: nil,
				curr: nil,
				len:  0,
			},
			want: nil,
		},
		{
			name: "Next",
			fields: fields{
				head: head,
				tail: tail,
				curr: head,
				len:  2,
			},
			want: tail,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Next(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Next() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Pop(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	var head = &Node{
		Data: &Commit{
			Message: "Head",
			UUID:    "1",
			Date:    time.Now(),
		},
		prev: nil,
	}

	var tail = &Node{
		Data: &Commit{
			Message: "Tail",
			UUID:    "3",
			Date:    time.Now(),
		},
		prev: head,
		next: nil,
	}
	head.next = tail

	tests := []struct {
		name   string
		fields fields
		want   *Node
		len    int
	}{
		{
			name: "Pop Empty List",
			fields: fields{
				head: nil,
				tail: nil,
				curr: nil,
				len:  0,
			},
			want: nil,
			len:  0,
		},
		{
			name: "Pop",
			fields: fields{
				head: head,
				tail: tail,
				curr: head,
				len:  2,
			},
			want: tail,
			len:  1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Pop(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Pop() = %v, want %v", got, tt.want)
			}
			if got := d.Len(); got != tt.len {
				t.Errorf("Len() = %v, want %v", got, tt.len)
			}

		})
	}
}

func TestDoubleLinkedList_Prepend(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}

	var head = &Node{
		Data: &Commit{
			Message: "Head",
			UUID:    "1",
			Date:    time.Now(),
		},
		prev: nil,
	}

	type args struct {
		c *Commit
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		len     int
		wantErr bool
	}{
		{
			name: "Prepend at empty list",
			fields: fields{
				head: nil,
				tail: nil,
				curr: nil,
				len:  0,
			},
			args: args{
				c: &Commit{
					Message: "Head",
					UUID:    "1",
					Date:    time.Now(),
				},
			},
			len:     1,
			wantErr: false,
		},
		{
			name: "Prepend",
			fields: fields{
				head: head,
				tail: head,
				curr: head,
				len:  1,
			},
			args: args{
				c: &Commit{
					Message: "Tail",
					UUID:    "2",
					Date:    time.Now(),
				},
			},
			len:     2,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if err := d.Prepend(tt.args.c); (err != nil) != tt.wantErr {
				t.Errorf("Prepend() error = %v, wantErr %v", err, tt.wantErr)
			}
			if got := d.Len(); got != tt.len {
				t.Errorf("Len() = %v, want %v", got, tt.len)
			}

		})
	}
}

func TestDoubleLinkedList_Prev(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}

	var head = &Node{
		Data: &Commit{
			Message: "Head",
			UUID:    "1",
			Date:    time.Now(),
		},
		prev: nil,
	}

	var tail = &Node{
		Data: &Commit{
			Message: "Tail",
			UUID:    "3",
			Date:    time.Now(),
		},
		prev: head,
		next: nil,
	}
	head.next = tail

	tests := []struct {
		name   string
		fields fields
		want   *Node
	}{
		{
			name: "Prev at empty list",
			fields: fields{
				head: nil,
				tail: nil,
				curr: nil,
				len:  0,
			},
			want: nil,
		},
		{
			name: "Prev",
			fields: fields{
				head: head,
				tail: tail,
				curr: tail,
				len:  2,
			},
			want: head,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Prev(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Prev() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Reverse(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}

	var head = &Node{
		Data: &Commit{
			Message: "Head",
			UUID:    "1",
			Date:    time.Now(),
		},
		prev: nil,
	}

	var tail = &Node{
		Data: &Commit{
			Message: "Tail",
			UUID:    "3",
			Date:    time.Now(),
		},
		prev: head,
		next: nil,
	}
	head.next = tail

	want := &DoubleLinkedList{
		head: tail,
		curr: head,
		tail: head,
		len:  2,
	}

	tests := []struct {
		name      string
		fields    fields
		want      *DoubleLinkedList
		wantError bool
	}{
		{
			name: "Reverse at empty list",
			fields: fields{
				head: nil,
				tail: nil,
				curr: nil,
				len:  0,
			},
			want:      &DoubleLinkedList{nil, nil, nil, 0},
			wantError: true,
		},
		{
			name: "Reverse",
			fields: fields{
				head: head,
				tail: tail,
				curr: head,
				len:  2,
			},
			want:      want,
			wantError: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Reverse(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Reverse() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Search(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}

	var head = &Node{
		Data: &Commit{
			Message: "Head",
			UUID:    "1",
			Date:    time.Now(),
		},
		prev: nil,
	}

	var tail = &Node{
		Data: &Commit{
			Message: "Tail",
			UUID:    "3",
			Date:    time.Now(),
		},
		prev: head,
		next: nil,
	}
	head.next = tail

	type args struct {
		message string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *Node
		wantErr bool
	}{
		{
			name: "Empty search",
			fields: fields{
				head: head,
				tail: tail,
				curr: head,
				len:  2,
			},
			args: args{
				message: "",
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Search",
			fields: fields{
				head: head,
				tail: tail,
				curr: head,
				len:  2,
			},
			args: args{
				message: "Tail",
			},
			want:    tail,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			got, err := d.Search(tt.args.message)
			if (err != nil) != tt.wantErr {
				t.Errorf("Search() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Search() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_SearchUUID(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}

	var head = &Node{
		Data: &Commit{
			Message: "Head",
			UUID:    "1",
			Date:    time.Now(),
		},
		prev: nil,
	}

	var tail = &Node{
		Data: &Commit{
			Message: "Tail",
			UUID:    "3",
			Date:    time.Now(),
		},
		prev: head,
		next: nil,
	}
	head.next = tail

	type args struct {
		uuID string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *Node
		wantErr bool
	}{
		{
			name: "Empty search",
			fields: fields{
				head: head,
				tail: tail,
				curr: head,
				len:  2,
			},
			args: args{
				uuID: "",
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Search",
			fields: fields{
				head: head,
				tail: tail,
				curr: head,
				len:  2,
			},
			args: args{
				uuID: "3",
			},
			want:    tail,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			got, err := d.SearchUUID(tt.args.uuID)
			if (err != nil) != tt.wantErr {
				t.Errorf("SearchUUID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("SearchUUID() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Shift(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}

	var head = &Node{
		Data: &Commit{
			Message: "Head",
			UUID:    "1",
			Date:    time.Now(),
		},
		prev: nil,
	}

	var tail = &Node{
		Data: &Commit{
			Message: "Tail",
			UUID:    "3",
			Date:    time.Now(),
		},
		prev: head,
		next: nil,
	}
	head.next = tail

	tests := []struct {
		name   string
		fields fields
		want   *Node
		len    int
	}{
		{
			name: "Shift Empty List",
			fields: fields{
				head: nil,
				tail: nil,
				curr: nil,
				len:  0,
			},
			want: nil,
			len:  0,
		},
		{
			name: "Shift",
			fields: fields{
				head: head,
				tail: tail,
				curr: head,
				len:  2,
			},
			want: head,
			len:  1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Shift(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Shift() = %v, want %v", got, tt.want)
			}
			if got := d.Len(); got != tt.len {
				t.Errorf("Len() = %v, want %v", got, tt.len)
			}

		})
	}
}

func TestGenerateJson(t *testing.T) {
	type args struct {
		count int
		path  string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "Genreate Json",
			args: args{
				count: count,
				path:  "./gen_test.json",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := GenerateJson(tt.args.count, tt.args.path); (err != nil) != tt.wantErr {
				t.Errorf("GenerateJson() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestQuickSort(t *testing.T) {
	type args struct {
		commits []*Commit
	}

	tests := []struct {
		name string
		args args
		want []*Commit
	}{
		{
			name: "QuickSort",
			args: args{
				commits: []*Commit{
					{
						Message: "3",
						UUID:    "3",
						Date:    time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC),
					},
					{
						Message: "2",
						UUID:    "2",
						Date:    time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC),
					},
					{
						Message: "1",
						UUID:    "1",
						Date:    time.Date(2021, 1, 1, 0, 0, 0, 0, time.UTC),
					},
					{
						Message: "4",
						UUID:    "4",
						Date:    time.Date(2024, 1, 1, 0, 0, 0, 0, time.UTC),
					},
				},
			},
			want: []*Commit{
				{
					Message: "1",
					UUID:    "1",
					Date:    time.Date(2021, 1, 1, 0, 0, 0, 0, time.UTC),
				},
				{
					Message: "2",
					UUID:    "2",
					Date:    time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC),
				},
				{
					Message: "3",
					UUID:    "3",
					Date:    time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC),
				},
				{
					Message: "4",
					UUID:    "4",
					Date:    time.Date(2024, 1, 1, 0, 0, 0, 0, time.UTC),
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := QuickSort(tt.args.commits); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("QuickSort() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestReadCommitsFromJson(t *testing.T) {
	type args struct {
		path string
	}

	tests := []struct {
		name    string
		args    args
		count   int
		wantErr bool
	}{
		{
			name: "ReadCommitsFromJson",
			args: args{
				path: "./gen_test.json",
			},
			count:   count,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ReadCommitsFromJson(tt.args.path)
			if (err != nil) != tt.wantErr {
				t.Errorf("ReadCommitsFromJson() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if len(got) != tt.count {
				t.Errorf("ReadCommitsFromJson() got = %v, want %v", len(got), tt.count)
			}
		})
	}
}
