package easy

var _ = defangIPaddr("1.1.1.1")

func defangIPaddr(address string) string {
	var defanged string
	for _, c := range address {
		if c == '.' {
			defanged += "[.]"
		} else {
			defanged += string(c)
		}
	}
	return defanged
}
