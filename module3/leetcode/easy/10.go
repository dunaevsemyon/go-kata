package easy

var _ = shuffle([]int{2, 5, 1, 3, 4, 7}, 3)

func shuffle(nums []int, n int) []int {
	shuffled := make([]int, 2*n)
	for i := 0; i < n; i++ {
		shuffled[2*i] = nums[i]
		shuffled[2*i+1] = nums[i+n]
	}
	return shuffled
}
