package easy

var _ = maximumWealth([][]int{{1, 2, 3}, {3, 2, 1}})

func maximumWealth(accounts [][]int) int {
	maxWealth := 0
	for i := 0; i < len(accounts); i++ {
		wealth := 0
		for j := 0; j < len(accounts[0]); j++ {
			wealth += accounts[i][j]
		}
		if wealth > maxWealth {
			maxWealth = wealth
		}
	}
	return maxWealth
}
