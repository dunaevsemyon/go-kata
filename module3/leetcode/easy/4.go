package easy

var _ = buildArray([]int{0, 2, 1, 5, 3, 4})

func buildArray(nums []int) []int {
	n := len(nums)
	ans := make([]int, n)
	for i := 0; i < n; i++ {
		ans[i] = nums[nums[i]]
	}
	return ans
}
