package easy

var _ = runningSum([]int{1, 1, 1, 1})

func runningSum(nums []int) []int {
	for i := 1; i < len(nums); i++ {
		nums[i] += nums[i-1]
	}
	return nums
}
