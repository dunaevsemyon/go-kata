package easy

var _ = decompressRLElist([]int{1, 2, 3, 4})

func decompressRLElist(nums []int) []int {
	var res []int

	for i := 0; i < len(nums); i += 2 {
		for freq := 0; freq < nums[i]; freq++ {
			res = append(res, nums[i+1])
		}
	}

	return res
}
