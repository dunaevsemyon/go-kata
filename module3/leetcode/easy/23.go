package easy

var _ = interpret("G()(al)")

func interpret(command string) string {
	target := ""
	i := 0
	n := len(command)

	for i < n {
		switch {
		case command[i:i+1] == "G":
			target += "G"
			i++
		case command[i:i+2] == "()":
			target += "o"
			i += 2
		default:
			target += "al"
			i += 4
		}
	}

	return target
}
