package easy

var _ = tribonacci(4)

func tribonacci(n int) int {
	if n == 0 {
		return 0
	} else if n == 1 || n == 2 {
		return 1
	}

	sum, n0, n1 := 1, 0, 1
	for i := 3; i <= n; i++ {
		n0, n1, sum = n1, sum, n0+n1+sum
	}

	return sum
}
