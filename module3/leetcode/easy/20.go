package easy

var _ = kidsWithCandies([]int{2, 3, 5, 1, 3}, 3)

func kidsWithCandies(candies []int, extraCandies int) []bool {
	maxCandies := 0
	for _, c := range candies {
		if c > maxCandies {
			maxCandies = c
		}
	}
	result := make([]bool, len(candies))
	for i, c := range candies {
		if c+extraCandies >= maxCandies {
			result[i] = true
		}
	}
	return result
}
