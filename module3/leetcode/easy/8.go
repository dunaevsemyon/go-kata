package easy

var _ = findKthPositive([]int{1, 2, 3, 4}, 2)

func findKthPositive(arr []int, k int) int {
	i, j := 0, 0
	for k > 0 {
		if i < len(arr) && arr[i] == j+1 {
			i++
		} else {
			k--
			if k == 0 {
				return j + 1
			}
		}
		j++
	}
	return j
}
