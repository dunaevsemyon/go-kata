package easy

var _ = createTargetArray([]int{0, 1, 2, 3, 4}, []int{0, 1, 2, 2, 1})

func createTargetArray(nums []int, index []int) []int {
	n := len(nums)

	arr := make([]int, n)
	for i := 0; i < n; i++ {
		idx := index[i]
		for j := n - 1; j > idx; j-- {
			arr[j] = arr[j-1]
		}
		arr[idx] = nums[i]
	}

	return arr
}
