package easy

var _ = decode([]int{1, 2, 3}, 1)

func decode(encoded []int, first int) []int {
	d := make([]int, len(encoded)+1)

	d[0] = first
	for i := 0; i < len(d)-1; i++ {
		d[i+1] = d[i] ^ encoded[i]
	}
	return d
}
