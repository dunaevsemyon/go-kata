package easy

var _ = balancedStringSplit("RLRRLLRLRL")

func balancedStringSplit(s string) int {
	r := 0
	l := 0

	maxBalanced := 0
	for _, n := range s {
		switch n {
		case 'R':
			r++
		case 'L':
			l++
		}

		if r == l {
			maxBalanced++
			r = 0
			l = 0
		}
	}

	return maxBalanced
}
