package easy

var _ = finalValueAfterOperations([]string{"--X", "X++", "X++"})

func finalValueAfterOperations(operations []string) int {
	var X int
	for _, op := range operations {
		switch op {
		case "++X", "X++":
			X++
		case "--X", "X--":
			X--
		}
	}
	return X
}
