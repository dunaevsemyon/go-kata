package easy

var _ = smallerNumbersThanCurrent([]int{8, 1, 2, 2, 3})

func smallerNumbersThanCurrent(nums []int) []int {
	output := make([]int, len(nums))
	tmpMap := map[int]int{}

	for i, num := range nums {
		if count, ok := tmpMap[num]; ok {
			output[i] = count
			continue
		}
		for _, value := range nums {
			if num > value {
				output[i]++
			}
		}
		tmpMap[num] = output[i]
	}
	return output
}
