package easy

import "strings"

var _ = mostWordsFound([]string{"alice and bob love leetcode", "i think so too", "this is great thanks very much"})

func mostWordsFound(sentences []string) int {
	maxWords := 0
	for _, sentence := range sentences {
		words := strings.Split(sentence, " ")
		wordCount := len(words)
		if wordCount > maxWords {
			maxWords = wordCount
		}
	}
	return maxWords
}
