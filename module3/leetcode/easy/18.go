package easy

var _ = differenceOfSum([]int{1, 15, 6, 3})

func differenceOfSum(nums []int) int {
	elementSum := 0
	digitSum := 0
	for _, num := range nums {
		elementSum += num
		for num > 0 {
			digitSum += num % 10
			num /= 10
		}
	}
	return abs(elementSum - digitSum)
}

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}
