package easy

var _ = subtractProductAndSum(234)

func subtractProductAndSum(n int) int {
	sum := 0
	mult := 1
	for n != 0 {
		digit := n % 10
		sum = sum + digit
		mult = mult * digit
		n = n / 10
	}

	return mult - sum
}
