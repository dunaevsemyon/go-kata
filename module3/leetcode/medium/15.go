package medium

var _ = minPartitions("32")

func minPartitions(n string) int {
	maxDigit := 0
	for _, c := range n {
		digit := int(c - '0')
		if digit > maxDigit {
			maxDigit = digit
		}
		if maxDigit == 9 {
			break
		}
	}
	return maxDigit
}
