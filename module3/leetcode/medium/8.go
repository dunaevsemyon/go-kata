package medium

var _ = findSmallestSetOfVertices(6, [][]int{{0, 1}, {0, 2}, {2, 5}, {3, 4}, {4, 2}})

// i dont know what i did exectly, but it works.
func findSmallestSetOfVertices(n int, edges [][]int) []int {
	arr := make([]int, n)
	for _, edge := range edges {
		arr[edge[1]]++
	}

	result := make([]int, 0)
	for i := 0; i < n; i++ {
		if arr[i] == 0 {
			result = append(result, i)
		}
	}

	return result
}
