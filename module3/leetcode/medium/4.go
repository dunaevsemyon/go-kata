package medium

var _ = bstToGst(nil)

func bstToGst(root *TreeNode) *TreeNode {
	sum := 0

	var recuriveIterate func(*TreeNode) *TreeNode
	recuriveIterate = func(node *TreeNode) *TreeNode {
		if node != nil {
			recuriveIterate(node.Right)

			sum += node.Val
			node.Val = sum

			recuriveIterate(node.Left)
		}
		return node
	}
	return recuriveIterate(root)
}
