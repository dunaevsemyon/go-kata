package medium

import (
	"sort"
)

var _ = sortTheStudents([][]int{nil}, 0)

func sortTheStudents(score [][]int, k int) [][]int {
	// SliceStable is used as recommended in the documentation
	sort.SliceStable(score, func(i, j int) bool {
		return score[i][k] > score[j][k]

	})
	return score
}
