package medium

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

var _ = deepestLeavesSum(nil)

func deepestLeavesSum(root *TreeNode) int {
	deepest := 0
	nodes := []*TreeNode{root}

	for len(nodes) > 0 {
		sum := 0
		n := len(nodes)

		for i := 0; i < n; i++ {
			node := nodes[0]
			nodes = nodes[1:]
			sum += node.Val

			if node.Left != nil {
				nodes = append(nodes, node.Left)
			}
			if node.Right != nil {
				nodes = append(nodes, node.Right)
			}
		}
		deepest = sum
	}
	return deepest
}
