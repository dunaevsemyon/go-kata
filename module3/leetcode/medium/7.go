package medium

var _ = balanceBST(nil)

func balanceBST(root *TreeNode) *TreeNode {
	nums := orderedSlice(root)
	return buildTree(nums, 0, len(nums)-1)
}

func orderedSlice(root *TreeNode) []int {
	if root == nil {
		return []int{}
	}
	left := orderedSlice(root.Left)
	right := orderedSlice(root.Right)
	return append(append(left, root.Val), right...)
}

func buildTree(nums []int, left, right int) *TreeNode {
	if left > right {
		return nil
	}
	mid := (left + right) / 2
	root := &TreeNode{Val: nums[mid]}
	root.Left = buildTree(nums, left, mid-1)
	root.Right = buildTree(nums, mid+1, right)
	return root
}
