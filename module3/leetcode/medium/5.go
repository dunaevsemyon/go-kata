package medium

var _ = pairSum(nil)

func pairSum(head *ListNode) int {

	var count int
	tmp := head
	for tmp != nil {
		count++
		tmp = tmp.Next
	}
	count /= 2

	var prev, next *ListNode
	curr := head

	for count > 0 {
		next = curr.Next
		curr.Next = prev
		prev = curr
		curr = next
		count--
	}

	var sum int

	for next != nil {
		if next.Val+prev.Val > sum {
			sum = next.Val + prev.Val
		}
		next = next.Next
		prev = prev.Next
	}

	return sum
}
