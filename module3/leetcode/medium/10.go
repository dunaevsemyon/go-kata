package medium

var _ = numTilePossibilities("AAB")

func numTilePossibilities(tiles string) int {
	counts := make(map[rune]int)
	for _, c := range tiles {
		counts[c]++
	}
	return variantCounter(counts)
}

func variantCounter(counts map[rune]int) int {
	count := 0
	for c, n := range counts {
		if n == 0 {
			continue
		}
		counts[c]--
		count++
		count += variantCounter(counts)
		counts[c]++
	}
	return count
}
