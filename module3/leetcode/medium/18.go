package medium

var _ = groupThePeople([]int{3, 3, 3, 3, 3, 1, 3})

func groupThePeople(groupSizes []int) [][]int {
	groups := make(map[int][]int)
	for i, size := range groupSizes {
		groups[size] = append(groups[size], i)
	}
	res := [][]int{}
	for size, members := range groups {
		for i := 0; i < len(members); i += size {
			res = append(res, members[i:i+size])
		}
	}
	return res
}
