package medium

var _ = mergeInBetween(nil, 0, 0, nil)

func mergeInBetween(list1 *ListNode, a int, b int, list2 *ListNode) *ListNode {
	var prev *ListNode
	curr := list1
	for i := 0; i < a; i++ {
		prev = curr
		curr = curr.Next
	}
	for i := a; i <= b; i++ {
		curr = curr.Next
	}
	prev.Next = list2
	for list2.Next != nil {
		list2 = list2.Next
	}
	list2.Next = curr
	return list1
}
