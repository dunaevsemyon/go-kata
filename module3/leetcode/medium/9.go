package medium

import "sort"

var _ = checkArithmeticSubarrays([]int{4, 6, 5, 9, 3, 7}, []int{0, 0, 2}, []int{2, 3, 5})

func checkArithmeticSubarrays(nums []int, l []int, r []int) []bool {
	var ans = make([]bool, len(l))

	for idx, val := range l {
		a := nums[val : r[idx]+1]
		var numsTmp = make([]int, len(a))
		copy(numsTmp, a)
		sort.Ints(numsTmp)
		ans[idx] = isArithmetic(numsTmp)
	}

	return ans
}

func isArithmetic(a []int) bool {
	diff := a[0] - a[1]

	for i := 1; i < len(a)-1; i++ {
		if a[i]-a[i+1] != diff {
			return false
		}
	}

	return true
}
