package medium

var _ = xorQueries([]int{1, 3, 4, 8}, [][]int{{0, 1}, {1, 2}, {0, 3}, {3, 3}})

func xorQueries(arr []int, queries [][]int) []int {
	n := len(arr)
	prefixXor := make([]int, n+1)
	for i := 0; i < n; i++ {
		prefixXor[i+1] = prefixXor[i] ^ arr[i]
	}
	m := len(queries)
	res := make([]int, m)
	for i := 0; i < m; i++ {
		left, right := queries[i][0], queries[i][1]
		res[i] = prefixXor[right+1] ^ prefixXor[left]
	}
	return res
}
