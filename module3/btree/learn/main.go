package main

import (
	"errors"
	"fmt"
)

type TreeNode struct {
	val   int
	left  *TreeNode
	right *TreeNode
}

func (t *TreeNode) Insert(value int) error {
	if t == nil {
		return errors.New("tree is nil")
	}

	if t.val == value {
		return errors.New("this node is already exists")
	}

	if t.val > value {
		if t.left == nil {
			t.left = &TreeNode{val: value}
			return nil
		}
		return t.left.Insert(value)
	}

	if t.val < value {
		if t.right == nil {
			t.right = &TreeNode{val: value}
			return nil
		}
		return t.right.Insert(value)
	}
	return nil
}

func (t *TreeNode) FindMin() int {
	if t.left == nil {
		return t.val
	}
	return t.left.FindMin()
}

func (t *TreeNode) FindMax() int {
	if t.right == nil {
		return t.val
	}
	return t.right.FindMax()
}

func (t *TreeNode) PrintOrder() {
	if t == nil {
		return
	}
	t.left.PrintOrder()
	fmt.Println(t.val)
	t.right.PrintOrder()
}

func main() {
	t := &TreeNode{val: 8}

	_ = t.Insert(1)
	_ = t.Insert(2)
	_ = t.Insert(3)
	_ = t.Insert(4)
	_ = t.Insert(5)
	_ = t.Insert(6)
	_ = t.Insert(7)

	fmt.Printf("Min: %d, Max: %d\n", t.FindMin(), t.FindMax())
	t.PrintOrder()
}
