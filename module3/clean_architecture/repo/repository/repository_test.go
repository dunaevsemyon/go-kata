package repository

import (
	"io"
	"log"
	"os"
	"reflect"
	"testing"
)

var testFile = func() (*os.File, error) {
	file, err := os.CreateTemp("", "users.json")
	if err != nil {
		return nil, err
	}
	defer os.Remove(file.Name())
	return file, err
}

func TestNewUserRepository(t *testing.T) {
	file, err := testFile()
	if err != nil {
		log.Println(err)
	}

	type args struct {
		file io.ReadWriteSeeker
	}
	tests := []struct {
		name string
		args args
		want *UserRepository
	}{
		{
			name: "valid file",
			args: args{
				file: file,
			},
			want: &UserRepository{
				File: file,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewUserRepository(tt.args.file); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewUserRepository() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserRepository_Save(t *testing.T) {
	file, err := testFile()
	if err != nil {
		log.Println(err)
	}

	type fields struct {
		File io.ReadWriteSeeker
	}
	type args struct {
		record interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "valid user",
			fields: fields{
				File: file,
			},
			args: args{
				record: User{
					ID:   1,
					Name: "John",
				},
			},
			wantErr: false,
		},
		{
			name: "invalid record",
			fields: fields{
				File: file,
			},
			args: args{
				record: "invalid record",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &UserRepository{
				File: tt.fields.File,
			}
			if err := r.Save(tt.args.record); (err != nil) != tt.wantErr {
				t.Errorf("UserRepository.Save() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestUserRepository_Find(t *testing.T) {
	file, err := testFile()
	if err != nil {
		log.Println(err)
	}

	repo := NewUserRepository(file)
	if err := repo.Save(User{
		ID:   1,
		Name: "John",
	}); err != nil {
		log.Println(err)
	}

	type fields struct {
		File io.ReadWriteSeeker
	}
	type args struct {
		id int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    interface{}
		wantErr bool
	}{
		{
			name: "valid user",
			fields: fields{
				File: file,
			},
			args: args{
				id: 1,
			},
			want: User{
				ID:   1,
				Name: "John",
			},
			wantErr: false,
		},
		{
			name: "non-existent user",
			fields: fields{
				File: file,
			},
			args: args{
				id: 3,
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &UserRepository{
				File: tt.fields.File,
			}
			got, err := r.Find(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("UserRepository.Find() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UserRepository.Find() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserRepository_FindAll(t *testing.T) {
	file, err := testFile()
	if err != nil {
		log.Println(err)
	}

	repo := NewUserRepository(file)
	if err := repo.Save(User{
		ID:   1,
		Name: "John",
	}); err != nil {
		log.Println(err)
	}
	if err := repo.Save(User{
		ID:   2,
		Name: "David",
	}); err != nil {
		log.Println(err)
	}

	type fields struct {
		File io.ReadWriteSeeker
	}
	tests := []struct {
		name    string
		fields  fields
		want    []interface{}
		wantErr bool
	}{
		{
			name: "valid users",
			fields: fields{
				File: file,
			},
			want: []interface{}{
				User{
					ID:   1,
					Name: "John",
				},
				User{
					ID:   2,
					Name: "David",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &UserRepository{
				File: tt.fields.File,
			}
			got, err := r.FindAll()
			if (err != nil) != tt.wantErr {
				t.Errorf("UserRepository.FindAll() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UserRepository.FindAll() = %v, want %v", got, tt.want)
			}
		})
	}
}
