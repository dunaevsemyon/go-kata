package repository

import (
	"encoding/json"
	"fmt"
	"io"
)

type Repository interface {
	Save(record interface{}) error
	Find(id int) (interface{}, error)
	FindAll() ([]interface{}, error)
}

type UserRepository struct {
	File io.ReadWriteSeeker
}

func NewUserRepository(file io.ReadWriteSeeker) *UserRepository {
	return &UserRepository{
		File: file,
	}
}

type User struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

// Загружать данные в конструкторе

func (r *UserRepository) Save(record interface{}) error {
	// logic to write a record as a JSON object to a file at r.FilePath
	user, ok := record.(User)
	if !ok {
		return fmt.Errorf("record is not a User")
	}

	users, err := r.FindAll()
	if err != nil {
		return err
	}

	for _, u := range users {
		if u.(User).ID == user.ID {
			return fmt.Errorf("ID: %d - already exists", user.ID)
		}
	}

	encoder := json.NewEncoder(r.File)
	return encoder.Encode(user)
}

func (r *UserRepository) Find(id int) (interface{}, error) {
	// logic to read a JSON object from a file at r.FilePath and return the corresponding record
	if _, err := r.File.Seek(0, io.SeekStart); err != nil {
		return nil, err
	}

	decoder := json.NewDecoder(r.File)
	for {
		var user User
		if err := decoder.Decode(&user); err != nil {
			if err == io.EOF {
				break
			}
			return nil, err
		}
		if user.ID == id {
			return user, nil
		}
	}

	return nil, fmt.Errorf("user not found")
}

func (r *UserRepository) FindAll() ([]interface{}, error) {
	// logic to read all JSON objects from a file at r.FilePath and return a slice of records
	if _, err := r.File.Seek(0, io.SeekStart); err != nil {
		return nil, err
	}
	decoder := json.NewDecoder(r.File)
	users := []interface{}{}
	for {
		var user User
		if err := decoder.Decode(&user); err != nil {
			if err == io.EOF {
				break
			}
			return nil, err
		}
		users = append(users, user)
	}

	return users, nil
}
