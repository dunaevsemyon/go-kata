package main

import (
	"gitlab.com/dunaevsemyon/go-kata/module3/clean_architecture/todo/infrastructure/presenter"
	"gitlab.com/dunaevsemyon/go-kata/module3/clean_architecture/todo/infrastructure/repostiory"
	"gitlab.com/dunaevsemyon/go-kata/module3/clean_architecture/todo/usecase"
)

func main() {
	repo := repostiory.NewFileDB("tasks.json")
	service := usecase.NewService(repo)
	cli := presenter.NewCli(service)

	cli.Run()
}
