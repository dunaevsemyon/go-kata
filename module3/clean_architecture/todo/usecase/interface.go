package usecase

import "gitlab.com/dunaevsemyon/go-kata/module3/clean_architecture/todo/entity"

type TaskRepo interface {
	Add(title, description string) (entity.Task, error)
	Remove(id int) (int, error)
	Update(task entity.Task) (entity.Task, error)
	Complete(id int) (int, error)
	List() ([]entity.Task, error)
}
