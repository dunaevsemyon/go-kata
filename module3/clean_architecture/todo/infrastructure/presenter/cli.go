package presenter

import (
	"fmt"
	"github.com/AlecAivazis/survey/v2"
	"gitlab.com/dunaevsemyon/go-kata/module3/clean_architecture/todo/entity"
	"gitlab.com/dunaevsemyon/go-kata/module3/clean_architecture/todo/usecase"
	"log"
)

type Cli struct {
	service usecase.Service
}

func NewCli(service *usecase.Service) *Cli {
	return &Cli{service: *service}
}

func (c *Cli) Run() {
	for {
		var choice string

		prompt := &survey.Select{
			Message: "Task Menu:",
			Options: []string{"List", "Add", "Update", "Complete", "Remove", "Exit"},
		}
		if err := survey.AskOne(prompt, &choice); err != nil {
			return
		}

		switch choice {
		case "List":
			c.List()
		case "Add":
			c.Add()
		case "Update":
			c.Update()
		case "Complete":
			c.Complete()
		case "Remove":
			c.Remove()
		case "Exit":
			return
		}
	}
}

func (c *Cli) List() {
	tasks, err := c.service.List()
	if err != nil {
		log.Println(err)
		return
	}
	fmt.Println("Tasks:")
	for _, task := range tasks {
		status := "[ ]"
		if task.Completed {
			status = "[X]"
		}
		fmt.Printf("%d. %s %s: %s\n", task.ID, status, task.Title,
			task.Description)
	}
}

func (c *Cli) Add() {
	prompt := []*survey.Question{
		{
			Name: "title",
			Prompt: &survey.Input{
				Message: "Title:",
			},
			Transform: survey.Title,
		},
		{
			Name: "description",
			Prompt: &survey.Input{
				Message: "Description:",
			},
			Transform: survey.Title,
		},
	}
	answer := struct{ Title, Description string }{}
	if err := survey.Ask(prompt, &answer); err != nil {
		log.Println(err)
		return
	}
	if answer.Title == "" || answer.Description == "" {
		log.Println("Title and description are required")
		return
	}
	task, err := c.service.Add(answer.Title, answer.Description)
	if err != nil {
		log.Println(err)
		return
	}
	fmt.Printf("Task %d: %s added\n", task.ID, task.Title)

}

func (c *Cli) Update() {
	tasks, err := c.service.List()
	if err != nil {
		log.Println(err)
		return
	}

	var taskTitles []string
	for _, task := range tasks {
		taskTitles = append(taskTitles, task.Title)
	}

	var selectedTitle string
	prompt := &survey.Select{
		Message: "Select task to update:",
		Options: taskTitles,
	}
	if err := survey.AskOne(prompt, &selectedTitle); err != nil {
		log.Println(err)
		return
	}

	var selectedTask entity.Task
	for _, task := range tasks {
		if task.Title == selectedTitle {
			selectedTask = task
			break
		}
	}

	prompts := []*survey.Question{
		{
			Name: "title",
			Prompt: &survey.Input{
				Message: "Title:",
				Default: selectedTask.Title,
			},
			Transform: survey.Title,
		},
		{
			Name: "description",
			Prompt: &survey.Input{
				Message: "Description:",
				Default: selectedTask.Description,
			},
			Transform: survey.Title,
		},
	}
	answer := struct{ Title, Description string }{}
	if err := survey.Ask(prompts, &answer); err != nil {
		log.Println(err)
		return
	}
	selectedTask.Title = answer.Title
	selectedTask.Description = answer.Description
	task, err := c.service.Update(selectedTask)
	if err != nil {
		log.Println(err)
		return
	}
	fmt.Printf("Task %d: %s updated\n", task.ID, task.Title)
}

func (c *Cli) Complete() {
	tasks, err := c.service.List()
	if err != nil {
		log.Println(err)
		return
	}
	var taskTitles []string
	for _, task := range tasks {
		taskTitles = append(taskTitles, task.Title)
	}

	var selectedTitle string
	prompt := &survey.Select{
		Message: "Select task to complete:",
		Options: taskTitles,
	}

	if err := survey.AskOne(prompt, &selectedTitle); err != nil {
		log.Println(err)
		return
	}

	var selectedTask entity.Task
	for _, task := range tasks {
		if task.Title == selectedTitle {
			selectedTask = task
			break
		}
	}

	_, err = c.service.Complete(selectedTask.ID)
	if err != nil {
		log.Println(err)
		return
	}
	fmt.Printf("Task %d: %s completed\n", selectedTask.ID, selectedTask.Title)
}

func (c *Cli) Remove() {
	tasks, err := c.service.List()
	if err != nil {
		log.Println(err)
		return
	}
	var taskTitles []string
	for _, task := range tasks {
		taskTitles = append(taskTitles, task.Title)
	}

	var selectedTitle string
	prompt := &survey.Select{
		Message: "Select task to remove:",
		Options: taskTitles,
	}

	if err := survey.AskOne(prompt, &selectedTitle); err != nil {
		log.Println(err)
		return
	}

	var selectedTask entity.Task
	for _, task := range tasks {
		if task.Title == selectedTitle {
			selectedTask = task
			break
		}
	}

	_, err = c.service.Remove(selectedTask.ID)
	if err != nil {
		log.Println(err)
		return
	}
	fmt.Printf("Task %d: %s removed\n", selectedTask.ID, selectedTask.Title)
}
