package main

import (
	"testing"
)

func BenchmarkNewFilterText(b *testing.B) {
	ft := NewFilterText()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		for i := range data {
			ft.SanitizeText(data[i])
		}
	}
}

func BenchmarkNewFilterText2(b *testing.B) {
	ft := NewFilterText2()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		for i := range data {
			ft.SanitizeText2(data[i])
		}
	}
}
