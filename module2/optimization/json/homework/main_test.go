package main

import "testing"

var jsonData = `
[{
	"id": 1,
	"name": "User Userov",
	"email": "user@users.com",
	"books": [{"name": "Azbuka"},{"name": "Toilet Paper"}]
},
{
	"id": 2,
	"name": "Grisha Grishkovez",
	"email": "grisha@users.com",
	"books": [{"name": "Local News"},{"name": "Remote News Paper"}]
},
{
	"id": 3,
	"name": "Ivan Ivanov",
	"email": "grisha@users.com",
	"books": [{"name": "War Thunder"},{"name": "It"}]
}
]`

var books, _ = UnmarshalBooksJsoniter([]byte(jsonData)) // use jsoniter coz of speed

func BenchmarkUnmarshalStl(b *testing.B) {
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, err := UnmarshalBooksStl([]byte(jsonData))
		if err != nil {
			panic(err)
		}
	}
}

func BenchmarkMarshalStl(b *testing.B) {
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		data, err := books.MarshalBooksStl()
		if err != nil {
			panic(err)
		}
		_ = data
	}

}

func BenchmarkUnmarshalJsoniter(b *testing.B) {
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, err := UnmarshalBooksJsoniter([]byte(jsonData))
		if err != nil {
			panic(err)
		}
	}
}

func BenchmarkMarshalJsoniter(b *testing.B) {
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		data, err := books.MarshalBooksJsoniter()
		if err != nil {
			panic(err)
		}
		_ = data
	}

}
