package main

import (
	"encoding/json"
	jsoniter "github.com/json-iterator/go"
)

type Users []User

type User struct {
	ID    int64  `json:"id"`
	Name  string `json:"name"`
	Email string `json:"email"`
	Books []Book `json:"books"`
}

type Book struct {
	Name string `json:"name"`
}

func UnmarshalBooksStl(data []byte) (Users, error) {
	var b Users
	err := json.Unmarshal(data, &b)
	return b, err
}

func (b *Users) MarshalBooksStl() ([]byte, error) {
	return json.Marshal(b)
}

func UnmarshalBooksJsoniter(data []byte) (Users, error) {
	var b Users
	var jsonJsoniter = jsoniter.ConfigCompatibleWithStandardLibrary
	err := jsonJsoniter.Unmarshal(data, &b)
	return b, err
}

func (b *Users) MarshalBooksJsoniter() ([]byte, error) {
	return jsoniter.Marshal(b)
}
