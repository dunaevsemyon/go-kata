package main

import (
	"math"
	"testing"
)

func Test_multiply(t *testing.T) {
	type args struct {
		a float64
		b float64
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{name: "1", args: args{a: 3, b: 0}, want: 0},
		{name: "2", args: args{a: 0, b: 3}, want: 0},
		{name: "3", args: args{a: 3, b: 3}, want: 9},
		{name: "4", args: args{a: 1, b: 3}, want: 3},
		{name: "5", args: args{a: 3, b: 1}, want: 3},
		{name: "6", args: args{a: 1, b: 1}, want: 1},
		{name: "7", args: args{a: 3, b: 5}, want: 15},
		{name: "8", args: args{a: 10, b: 0.5}, want: 5},
		{name: "9", args: args{a: 3, b: -3}, want: -9},
		{name: "10", args: args{a: -3, b: -3}, want: 9},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := multiply(tt.args.a, tt.args.b); got != tt.want {
				t.Errorf("multiply() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_divide(t *testing.T) {
	type args struct {
		a float64
		b float64
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{name: "1", args: args{a: 3, b: 0}, want: math.Inf(1)},
		{name: "2", args: args{a: 0, b: 3}, want: 0},
		{name: "3", args: args{a: 3, b: 3}, want: 1},
		{name: "4", args: args{a: 3, b: 1}, want: 3},
		{name: "5", args: args{a: 1, b: 1}, want: 1},
		{name: "6", args: args{a: 9, b: 3}, want: 3},
		{name: "7", args: args{a: 15, b: 5}, want: 3},
		{name: "8", args: args{a: 10, b: 0.5}, want: 20},
		{name: "9", args: args{a: 3, b: -3}, want: -1},
		{name: "10", args: args{a: -3, b: -3}, want: 1},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := divide(tt.args.a, tt.args.b); got != tt.want {
				t.Errorf("divide() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_sum(t *testing.T) {
	type args struct {
		a float64
		b float64
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{name: "1", args: args{a: 3, b: 0}, want: 3},
		{name: "2", args: args{a: 0, b: 3}, want: 3},
		{name: "3", args: args{a: 3, b: 3}, want: 6},
		{name: "4", args: args{a: 1, b: 3}, want: 4},
		{name: "5", args: args{a: 3, b: 1}, want: 4},
		{name: "6", args: args{a: 1, b: 1}, want: 2},
		{name: "7", args: args{a: 3, b: 5}, want: 8},
		{name: "8", args: args{a: 10, b: 0.5}, want: 10.5},
		{name: "9", args: args{a: 3, b: -3}, want: 0},
		{name: "10", args: args{a: -3, b: -3}, want: -6},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := sum(tt.args.a, tt.args.b); got != tt.want {
				t.Errorf("sum() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_average(t *testing.T) {
	type args struct {
		a float64
		b float64
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{name: "1", args: args{a: 3, b: 0}, want: 1.5},
		{name: "2", args: args{a: 0, b: 3}, want: 1.5},
		{name: "3", args: args{a: 3, b: 3}, want: 3},
		{name: "4", args: args{a: 1, b: 3}, want: 2},
		{name: "5", args: args{a: 3, b: 1}, want: 2},
		{name: "6", args: args{a: 1, b: 1}, want: 1},
		{name: "7", args: args{a: 3, b: 5}, want: 4},
		{name: "8", args: args{a: 10, b: 0.5}, want: 5.25},
		{name: "9", args: args{a: 3, b: -3}, want: 0},
		{name: "10", args: args{a: -3, b: -3}, want: -3},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := average(tt.args.a, tt.args.b); got != tt.want {
				t.Errorf("average() = %v, want %v", got, tt.want)
			}
		})
	}
}
