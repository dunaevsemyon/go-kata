package main

import "fmt"

type Calc struct {
	a, b   float64
	result float64
}

func NewCalc() *Calc {
	return &Calc{}
}

func (c *Calc) SetA(a float64) *Calc {
	(*c).a = a
	return c
}

func (c *Calc) SetB(b float64) *Calc {
	(*c).b = b

	return c
}

func (c *Calc) Do(operation func(a, b float64) float64) *Calc {
	(*c).result = operation(c.a, c.b)

	return c
}

func (c *Calc) Result() float64 {
	return c.result
}

// multiplication
func multiply(a, b float64) float64 {
	return a * b
}

// divide
func divide(a, b float64) float64 {
	return a / b
}

// sum
func sum(a, b float64) float64 {
	return a + b
}

// average
func average(a, b float64) float64 {
	return (a + b) / 2
}

func main() {
	calc := NewCalc()
	res := calc.SetA(10).SetB(34).Do(multiply).Result()
	fmt.Println(res)

	res2 := calc.Result()
	fmt.Println(res2)

	if res != res2 {
		panic("object statement is not persist")
	}
}
