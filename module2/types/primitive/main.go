package main

import (
	"fmt"
	"unsafe"
)

func main() {
	n := 112358132134
	fmt.Println("size is:", unsafe.Sizeof(n), "bytes")
	typeInt()
	typeUint()
	typeFloat()
	typeByte()
	typeBool()
}

func typeInt() {
	fmt.Println("=== START type int ===")

	var uintNumber8 uint8 = 1 << 7
	var min8 = int8(uintNumber8)
	uintNumber8--
	var max8 = int8(uintNumber8)
	fmt.Println(min8, max8)
	fmt.Println("int8 min value:", min8, "int8 max value:", max8, "size:", unsafe.Sizeof(min8), "bytes")

	fmt.Println("=== END type int ===")
}

func typeUint() {
	fmt.Println("=== START type uint ===")

	var numberUint8 uint8 = 1 << 1
	fmt.Println("left shift uint8 is:", numberUint8, "size:", unsafe.Sizeof(numberUint8), "bytes")

	numberUint8 = (1 << 8) - 1
	fmt.Println("uint8 max value:", numberUint8, "size:", unsafe.Sizeof(numberUint8), "bytes")

	var numberUint16 uint16 = (1 << 16) - 1
	fmt.Println("uint16 max value:", numberUint16, "size:", unsafe.Sizeof(numberUint16), "bytes")

	var numberUint32 uint32 = (1 << 32) - 1
	fmt.Println("uint32 max value:", numberUint32, "size:", unsafe.Sizeof(numberUint32), "bytes")

	var numberUint64 uint64 = (1 << 64) - 1
	fmt.Println("uint64 max value:", numberUint64, "size:", unsafe.Sizeof(numberUint64), "bytes")

	fmt.Println("=== END type uint ===")
}

func typeFloat() {
	fmt.Println("=== START type float ===")

	// рассчитаем экспоненту
	var uintNumber uint32 = 1 << 29
	uintNumber += 1 << 28
	uintNumber += 1 << 27
	uintNumber += 1 << 26

	// мантисаа
	uintNumber += 1 << 21

	// старший бит для знака минус
	uintNumber += 1 << 31

	var floatNumber float32 = *(*float32)(unsafe.Pointer(&uintNumber))

	fmt.Println(floatNumber)

	fmt.Println("=== END type float ===")
}

func typeByte() {
	fmt.Println("=== START type byte ===")

	var b byte = 124
	fmt.Println("size:", unsafe.Sizeof(b), "bytes")

	fmt.Println("=== END type byte ===")
}

func typeBool() {
	fmt.Println("=== START type bool ===")

	var b bool
	fmt.Println("size:", unsafe.Sizeof(b), "bytes")
	var u uint8 = 1
	b = *(*bool)(unsafe.Pointer(&u))
	fmt.Println(b)

	fmt.Println("=== END type bool ===")
}
