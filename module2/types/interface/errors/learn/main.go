package main

import (
	"errors"
	"fmt"
)

type WrapError struct {
	Context string
	Err     error
}

func (w *WrapError) Error() string {
	return fmt.Sprintf("%s: %v", w.Context, w.Err)
}

func Wrap(err error, info string) *WrapError {
	return &WrapError{
		Context: info,
		Err:     err,
	}
}

func main() {
	err := errors.New("boom")
	err = Wrap(err, "main")
	fmt.Println(err)
}
