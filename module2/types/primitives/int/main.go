package main

import (
	"fmt"
	"unsafe"
)

func main() {
	n := 112358132134
	fmt.Println("size is:", unsafe.Sizeof(n), "bytes")
	typeInt()
}

func typeInt() {
	fmt.Println("=== START type int ===")

	var uintNumber8 uint8 = 1 << 7
	var min8 = int8(uintNumber8)
	uintNumber8--
	var max8 = int8(uintNumber8)
	fmt.Println(min8, max8)
	fmt.Println("int8 min value:", min8, "int8 max value:", max8, "size:", unsafe.Sizeof(min8), "bytes")

	fmt.Println("=== END type int ===")
}
