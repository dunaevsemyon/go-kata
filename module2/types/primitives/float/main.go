package main

import (
	"fmt"
	"unsafe"
)

func main() {
	n := 112358132134
	fmt.Println("size is:", unsafe.Sizeof(n), "bytes")
	typeFloat()
}

func typeFloat() {
	fmt.Println("=== START type float ===")

	// рассчитаем экспоненту
	var uintNumber uint32 = 1 << 29
	uintNumber += 1 << 28
	uintNumber += 1 << 27
	uintNumber += 1 << 26

	// мантисаа
	uintNumber += 1 << 21

	// старший бит для знака минус
	uintNumber += 1 << 31

	var floatNumber float32 = *(*float32)(unsafe.Pointer(&uintNumber))

	fmt.Println(floatNumber)

	fmt.Println("=== END type float ===")
}
