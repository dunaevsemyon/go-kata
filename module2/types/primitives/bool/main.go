package main

import (
	"fmt"
	"unsafe"
)

func main() {
	n := 112358132134
	fmt.Println("size is:", unsafe.Sizeof(n), "bytes")
	typeBool()
}

func typeBool() {
	var b bool
	fmt.Println("size:", unsafe.Sizeof(b), "bytes")
	var u uint8 = 1
	b = *(*bool)(unsafe.Pointer(&u))
	fmt.Println(b)
}
