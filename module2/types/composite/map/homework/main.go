package main

import (
	"fmt"
	"path"
)

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose",
			Stars: 27600,
		},
		{
			Name:  "https://github.com/erikw/tmux-powerline",
			Stars: 3000,
		},
		{
			Name:  "https://github.com/freeCodeCamp/freeCodeCamp",
			Stars: 359563,
		},
		{
			Name:  "https://github.com/EbookFoundation/free-programming-books",
			Stars: 261856,
		},
		{
			Name:  "https://github.com/jwasham/coding-interview-university",
			Stars: 244415,
		},
		{
			Name:  "https://github.com/sindresorhus/awesome",
			Stars: 234345,
		},
		{
			Name:  "https://github.com/kamranahmedse/developer-roadmap",
			Stars: 225299,
		},
		{
			Name:  "https://github.com/public-apis/public-apis",
			Stars: 224655,
		},
		{
			Name:  "https://github.com/donnemartin/system-design-primer",
			Stars: 208537,
		},
		{
			Name:  "https://github.com/vuejs/vue",
			Stars: 201801,
		},
		{
			Name:  "https://github.com/facebook/react",
			Stars: 200516,
		},
		{
			Name:  "https://github.com/tensorflow/tensorflow",
			Stars: 170412,
		},
		{
			Name:  "https://github.com/golang/go",
			Stars: 107546,
		},
	}

	// в цикле запишите в map
	repoStars := make(map[string]Project)

	for _, i := range projects {
		name := path.Base(i.Name)
		repoStars[name] = i
	}
	// в цикле пройдитесь по мапе и выведите значения в консоль
	for r, s := range repoStars {
		fmt.Printf("Name: %s\nUrl: %v\nStars: %d\n---\n", r, s.Name, s.Stars)
	}
}
