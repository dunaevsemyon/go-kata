package main

import (
	"fmt"
)

func main() {
	s := []int{1, 2, 3}
	Append(&s)
	fmt.Println(s)
}

func Append(s *[]int) {
	newS := make([]int, len(*s)+1)
	copy(newS, *s)
	newS[len(*s)] = 4
	*s = newS
}
