package main

import (
	"fmt"
	"unicode/utf8"
)

func main() {
	str := []byte("नमस्ते दुनिया") // Hello World in Hindi

	for len(str) > 0 {
		r, size := utf8.DecodeRune(str)
		fmt.Printf("%c %v bytes\n", r, size)

		str = str[size:]
	}
}
