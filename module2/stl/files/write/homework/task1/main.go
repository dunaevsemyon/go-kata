package main

import (
	"bufio"
	"fmt"
	"os"
)

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {

	reader := bufio.NewReader(os.Stdin)

	fmt.Print("Enter your name: ")

	name, _ := reader.ReadString('\n')
	fmt.Printf("Hello %s\n", name)

	f, err := os.Create("./names")
	check(err)

	defer f.Close()

	b, err := f.WriteString(name)
	fmt.Printf("wrote %d bytes\n", b)
	check(err)

	err = f.Sync()
	check(err)
}
