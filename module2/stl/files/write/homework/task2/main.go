package main

import (
	// "fmt"
	"os"

	"github.com/essentialkaos/translit/v2"
)

const (
	examplePath          = "./example.txt"
	exampleProcessedPath = "./example.processed.txt"
)

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	bText, err := os.ReadFile(examplePath)
	checkErr(err)

	text := string(bText)
	// fmt.Println(text)

	processedText := translit.EncodeToBGN(text)
	// fmt.Println(processedText)

	err = os.WriteFile(exampleProcessedPath, []byte(processedText), 0644)
	checkErr(err)
}
