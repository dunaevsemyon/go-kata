package main

import "fmt"

func main() {
	story := generateSelfStory("Ivan", 35, 120.5)
	fmt.Println(story)
}

func generateSelfStory(name string, age int, money float64) string {
	result := fmt.Sprintf("Hello! My name is %v. I'm %d y.o. And I also have $%0.2f in my wallet right now", name, age, money)
	return result
}
