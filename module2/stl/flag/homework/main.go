package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
	// "io"
)

// structure to json: type Config struct { AppName    string `json:"AppName"` Production bool   `json:"Production"` }

type Config struct {
	AppName    string `json:"AppName"`
	Production bool   `json:"Production"`
}

func main() {
	var configPath string

	flag.StringVar(&configPath, "conf", "config.json", "path to config.json file")
	flag.Parse()

	f, err := os.Open(configPath)
	if err != nil {
		fmt.Println("open config file failed, err:", err)
	}
	defer f.Close()

	// read file and unmarshal to structure Config
	conf := Config{}
	if err := json.NewDecoder(f).Decode(&conf); err != nil {
		fmt.Println("Error: ", err)
	}

	fmt.Printf("Production: %t\nAppName: %s\n", conf.Production, conf.AppName)
}
