package main

import (
	"bytes"
	"fmt"
	"io"
	"os"
)

func main() {
	data := []string{
		"there is 3pm, but im still alive to write this code snippet",
		"чистый код лучше, чем заумный код",
		"ваш код станет наследием будущих программистов",
		"задумайтесь об этом",
	}
	// Создай bytes.Buffer...
	var b bytes.Buffer
	// ...и запиши в него строки.
	for _, v := range data {
		b.WriteString(v)
		b.WriteString("\n")
	}

	// Создай файл example.txt
	f, err := os.Create("example.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()
	// запиши строки в файл
	if _, err := io.Copy(f, &b); err != nil {
		panic(err)
	}

	// Прочти данные файла в новый буфер
	var b2 bytes.Buffer

	f, err = os.Open("example.txt")
	if err != nil {
		panic(err)
	}
	_, err = io.Copy(&b2, f)
	if err != nil {
		panic(err)
	}
	// В конце программы выведи данные из нового буфера через fmt.Println.
	fmt.Println(b2.String())

}
