package main

import (
	"database/sql"
	"fmt"

	_ "github.com/mattn/go-sqlite3"
)

type User struct {
	Name  string
	Email string
}

func main() {
	database, _ := sql.Open("sqlite3", "./database.db")
	users := []User{
		{Name: "John Doe", Email: "john@doe.com"},
		{Name: "Jane Doe", Email: "jane@doe.com"},
		{Name: "Jack Doe", Email: "jack@doe.com"},
		{Name: "Maria Doe", Email: "jill@doe.com"},
	}

	statement, _ := database.Prepare("CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, email TEXT)")
	if _, err := statement.Exec(); err != nil {
		panic(err)
	}

	statement, _ = database.Prepare("INSERT INTO users (name, email) VALUES (?, ?)")
	for i := range users {
		_, err := statement.Exec(users[i].Name, users[i].Email)
		if err != nil {
			panic(err)
		}
	}

	row, _ := database.Query("SELECT id, name, email FROM users")
	var id int
	var name string
	var email string

	for row.Next() {
		if err := row.Scan(&id, &name, &email); err != nil {
			panic(err)
		}
		fmt.Printf("%d: %s %s\n", id, name, email)
	}
}
