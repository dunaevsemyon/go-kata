package main

import (
	"errors"
	"fmt"
)

// MergeDictsJob is a job to merge dictionaries into a single dictionary.
type MergeDictsJob struct {
	Dicts      []map[string]string
	Merged     map[string]string
	IsFinished bool
}

// errors
var (
	errNotEnoughDicts = errors.New("at least 2 dictionaries are required")
	errNilDict        = errors.New("nil dictionary")
	errNo             = errors.New("no errors found") // add just for pretty output
)

// job method for IsFinished
func (j *MergeDictsJob) setFinished() {
	j.IsFinished = true
}

func ExecuteMergeDictsJob(job *MergeDictsJob) (*MergeDictsJob, error) {
	defer job.setFinished()

	if len(job.Dicts) < 2 {
		return job, errNotEnoughDicts
	}

	job.Merged = make(map[string]string)
	for _, dict := range job.Dicts {
		// check if nil
		if dict == nil {
			return job, errNilDict
		}
		// fill job.Merged
		for k, v := range dict {
			job.Merged[k] = v
		}
	}

	return job, errNo
}

// Пример работы
func main() {
	testDicts := [][]map[string]string{{{}}, {{"a": "b"}, nil}, {{"a": "b"}, {"b": "c"}}}
	for i, v := range testDicts {
		mdj, err := ExecuteMergeDictsJob(&MergeDictsJob{Dicts: v})
		fmt.Printf("test %d\nMerged: %v\nError: %s\n\n", i, mdj.Merged, err)
	}
	// ExecuteMergeDictsJob(&MergeDictsJob{}) // &MergeDictsJob{IsFinished: true}, "at least 2 dictionaries are required"
	// ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"},nil}}) // &MergeDictsJob{IsFinished: true, Dicts: []map[string]string{{"a": "b"},nil}}, "nil dictionary"
	// ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"},{"b": "c"}}}) // &MergeDictsJob{IsFinished: true, Dicts: []map[string]string{{"a": "b", "b": "c"}}}, nil
}
