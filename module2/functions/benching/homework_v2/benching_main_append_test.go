package main

import (
	"testing"
)

// function benchmark for MapUserProducts
func BenchmarkMapUserProducts(b *testing.B) {
	users := genUsers()
	products := genProducts()
	for i := 0; i < b.N; i++ {
		MapUserProducts(users, products)
	}
}
func BenchmarkMapUserProducts2(b *testing.B) {
	users := genUsers()
	products := genProducts()
	for i := 0; i < b.N; i++ {
		MapUserProducts2(users, products)
	}
}

func BenchmarkMapUserProducts3(b *testing.B) {
	users := genUsers()
	products := genProducts()
	for i := 0; i < b.N; i++ {
		MapUserProducts3(users, products)
	}
}
