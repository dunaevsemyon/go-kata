package main

import (
	"fmt"
	"unicode/utf8"
)

func Greet(name string) string {
	// we can check runeCount length to detect russian
	if len(name) == utf8.RuneCountInString(name) {
		return fmt.Sprintf("Hello %s, you welcome!", name)
	}
	return fmt.Sprintf("Привет %s, добро пожаловать!", name)
}

func main() {
	var name string
	fmt.Scanln(&name)
	fmt.Println(Greet(name))
}
