package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestGreeting(t *testing.T) {
	tests := []struct {
		name string
		want string
	}{
		// DONE: Add test cases.
		{name: "Hello", want: "Hello"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Greeting(); got != tt.want {
				t.Errorf("Greeting() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGoodBye(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		// DONE: Add test cases.
		{name: "Goodbye, John", args: args{name: "John"}, want: "Goodbye, John..."},
		{name: "Goodbye, David", args: args{name: "David"}, want: "Goodbye, David..."},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GoodBye(tt.args.name); got != tt.want {
				t.Errorf("GoodBye() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAdd(t *testing.T) {
	type args struct {
		a int
		b int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		// DONE: Add test cases.
		{
			name: "a equal b",
			args: args{a: 2, b: 2},
			want: 4,
		},
		{
			name: "a less tnan b",
			args: args{a: 1, b: 3},
			want: 4,
		},
		{
			name: "a bigger than b",
			args: args{a: 3, b: 1},
			want: 4,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Add(tt.args.a, tt.args.b); got != tt.want {
				t.Errorf("Add() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGreetingAPI(t *testing.T) {

	req, err := http.NewRequest("GET", "/", nil)

	if err != nil {
		t.Fatal(err)
	}

	recorder := httptest.NewRecorder()

	handler := http.HandlerFunc(GreetingAPI)

	handler.ServeHTTP(recorder, req)

	if status := recorder.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	expected := "Hello Gopher" // expected response from the API call

	if recorder.Body.String() != expected { // check if the response is as expected
		t.Errorf("handler returned unexpected body: got %v want %v", recorder.Body.String(), expected) // if not, throw an error
	}
}
