package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"time"
)

type userID int

const (
	userIDctx userID = 0
)

func main() {
	http.HandleFunc("/", handle)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func handle(w http.ResponseWriter, r *http.Request) {
	id := r.Header.Get("User-id")

	ctx := context.WithValue(r.Context(), userIDctx, id)

	result := processLongTask(ctx)
	_, err := w.Write([]byte(result))
	if err != nil {
		log.Println(err)
	}
}

func processLongTask(ctx context.Context) string {
	id := ctx.Value(userIDctx)
	select {
	case <-time.After(5 * time.Second):
		return fmt.Sprintf("done task id: %v\n", id)
	case <-ctx.Done():
		log.Println("request cancelled")
		return ""
	}
}
