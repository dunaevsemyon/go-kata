package main

import (
	"fmt"
	"log"
	"math/rand"
	"runtime"
	"time"
)

func SayGreetings(greeting string, times int) {
	for i := 0; i < times; i++ {
		log.Println(greeting)
		d := time.Second * time.Duration(rand.Intn(5)) / 2
		time.Sleep(d)
	}
}

func main() {
	rand.NewSource(time.Now().UnixNano())
	log.SetFlags(0)
	go SayGreetings("hi!", 10)
	go SayGreetings("hello!", 10)
	go SayGreetings("Hello World!", 10)
	time.Sleep(time.Second * 2)

	fmt.Println(runtime.NumCPU())

	PanicExample()
}

func PanicExample() {
	defer func() {
		fmt.Println("exit normally.")
	}()
	fmt.Println("before panic.")
	defer func() {
		v := recover()
		fmt.Println("recovered:", v)
	}()
	panic("bye!")
	// fmt.Println("unreachable.")
}
