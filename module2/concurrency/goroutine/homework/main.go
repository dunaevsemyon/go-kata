package main

import (
	"fmt"
	// "net/http"
	// "sync"

	//"math/rand"
	"time"
)

func main() {
	message1 := make(chan string)
	message2 := make(chan string)

	go func() {
		for {
			time.Sleep(time.Millisecond * 500)
			message1 <- "half second pass"
		}
	}()

	go func() {
		for {
			time.Sleep(time.Second * 2)
			message2 <- "2 seconds pass"
		}
	}()

	for {
		select {
		case msg := <-message1:
			fmt.Println(msg)
		case msg := <-message2:
			fmt.Println(msg)
		}
	}
}

////////////////////////////////
// func main() {
// 	urls := []string{
// 		"https://google.com",
// 		"https://ya.ru",
// 		"https://github.com",
// 		"https://medium.com",
// 		"https://linux.org.ru",
// 	}

// 	var wg sync.WaitGroup

// 	for _, url := range urls {
// 		wg.Add(1)
// 		go func(url string) {
// 			doHTTP(url)
// 			wg.Complete()
// 		}(url)
// 	}

// 	wg.Wait()
// }

// func doHTTP(url string) {
// 	t := time.Now()

// 	resp, err := http.Get(url)
// 	if err != nil {
// 		fmt.Printf("failed to get <%s>: %s", url, err.Error())
// 	}

// 	defer resp.Body.Close()

// 	fmt.Printf("<%s> - Status Code [%d] - Latency %d ms\n", url, resp.StatusCode, time.Since(t).Milliseconds())
// }

////////////////////////////////
// func main() {
// 	message := make(chan string, 2) // bufferized channel with len 2
// 	message <- "hello"
// 	message <- "hello"
// 	// message <- "hello" // deadlock
// 	fmt.Println(<-message) // read buffer
// 	message <- "hello"     // no deadlock coz of read

// 	fmt.Println(<-message)
// 	fmt.Println(<-message)
// 	// fmt.Println(<-message) // deadlock
// }

////////////////////////////////
// func main() {
// 	message := make(chan string)
// 	go func() {
// 		for i := 1; i <= 10; i++ {
// 			message <- fmt.Sprintf("%d", i)
// 			time.Sleep(time.Millisecond * 500)
// 		}

// 		close(message)
// 	}()

// 	for msg := range message {
// 		fmt.Println(msg)
// 	}
// }

////////////////////////////////
// func main() {
// 	message := make(chan string)

// 	go func() {
// 		time.Sleep(2 * time.Second)
// 		message <- "hello chanel string"
// 	}()

// 	fmt.Println(<-message)
// }

////////////////////////////////
// func main() {
// 	t := time.Now()

// 	rand.NewSource(t.UnixNano())

// 	go parseURL("google.com")
// 	parseURL("yandex.com")

// 	fmt.Printf("parsing comleted. time elapsed: %0.2f seconds\n", time.Since(t).Seconds())

// }

////////////////////////////////
// func parseURL(url string) {
// 	for i := 0; i < 5; i++ {
// 		latency := rand.Intn(500) + 500

// 		time.Sleep(time.Duration(latency) * time.Millisecond)

// 		fmt.Printf("Parsing <%s> - Step %d - Latency %d\n", url, i+1, latency)

// 	}
// }
