package main

func main() {
	ch := make(chan uint32, 4)
	ch <- 1
	ch <- 2
	//_ = <-ch
	ch <- 3

	close(ch)
}
