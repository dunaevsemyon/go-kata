package main

import "fmt"

type Node struct {
	next *Node
	//prev *Node // unused
	//a, b      int // unused
	operation func(a, b int) int
	inter     map[string]*Node
	name      string
	length    float64
}

func main() {

	var length float64
	ram := &Node{name: "ram", length: 0.2}
	alu := &Node{name: "alu", length: 0.001}
	cache := &Node{name: "cache", length: 0.001}
	cpu := &Node{name: "cpu"}
	cpu.inter = make(map[string]*Node)
	cpu.inter["cache"] = cache
	cpu.inter["ram"] = ram
	cpu.inter["alu"] = alu
	alu.next = cpu
	cache.next = cpu
	ram.next = cpu
	alu.operation = func(a, b int) int {
		return a + b
	}

	var operand *Node
	var a, b int
	a, b = 5, 6
	operand = ram
	fmt.Println(operand.name)
	length += operand.length //nolint
	operand = operand.next
	fmt.Println(operand.name)
	operand = operand.inter["alu"]
	fmt.Println(operand.name)
	length += operand.length //nolint
	res := operand.operation(a, b)
	length += operand.length //nolint
	operand = operand.next
	fmt.Println(operand.name)
	operand = operand.inter["ram"]
	length += operand.length //nolint
	fmt.Println(operand.name)

	fmt.Println(cpu)
	fmt.Println(res)
}
