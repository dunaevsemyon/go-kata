package handlers

import (
	"encoding/json"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"gitlab.com/dunaevsemyon/go-kata/module4/webserver/http/homework/models"
	"gitlab.com/dunaevsemyon/go-kata/module4/webserver/http/homework/service"
	"net/http"
	"strconv"
)

type UserHandler struct {
	repoService service.UserService
}

func New(rs service.UserService) *UserHandler {
	return &UserHandler{
		repoService: rs,
	}
}

func (h *UserHandler) Hello(w http.ResponseWriter, r *http.Request) {
	render.JSON(w, r, "*Hello and Welcome* page")
}

func (h *UserHandler) Users(w http.ResponseWriter, r *http.Request) {
	users := h.repoService.Users()
	render.JSON(w, r, users)
}

func (h *UserHandler) User(w http.ResponseWriter, r *http.Request) {
	id, _ := strconv.Atoi(chi.URLParam(r, "id"))
	user, err := h.repoService.User(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	render.JSON(w, r, user)
}

func (h *UserHandler) Add(w http.ResponseWriter, r *http.Request) {
	var user models.User
	_ = json.NewDecoder(r.Body).Decode(&user)
	h.repoService.Add(user)
	w.WriteHeader(http.StatusCreated)
}
