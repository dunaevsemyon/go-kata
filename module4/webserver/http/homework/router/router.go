package router

import (
	"github.com/go-chi/chi/v5"
	"gitlab.com/dunaevsemyon/go-kata/module4/webserver/http/homework/handlers"
)

func RegisterRoutes(router chi.Router, h *handlers.UserHandler) {
	router.Get("/", h.Hello)
	router.Route("/users", func(r chi.Router) {
		r.Get("/", h.Users)
		r.Get("/{id}", h.User)
		r.Post("/", h.Add)
	})

	router.Route("/upload", func(r chi.Router) {
		r.Post("/", h.Upload)
		r.Get("/{fileName}", h.Download)
	})
}
