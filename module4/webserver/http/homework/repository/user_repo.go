package repository

import (
	"fmt"
	"gitlab.com/dunaevsemyon/go-kata/module4/webserver/http/homework/models"
)

type UserRepo struct {
	users []models.User
}

func New() *UserRepo {
	return &UserRepo{
		users: []models.User{},
	}
}

func (r *UserRepo) Users() []models.User {
	return r.users
}

func (r *UserRepo) User(id int) (models.User, error) {
	var user models.User
	for _, u := range r.users {
		if u.ID == id {
			user = u
			return user, nil
		}
	}
	return user, fmt.Errorf("user %d not found", id)
}

func (r *UserRepo) Add(user models.User) models.User {
	user.ID = len(r.users) + 1
	r.users = append(r.users, user)
	return user
}
