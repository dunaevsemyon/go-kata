package service

import "gitlab.com/dunaevsemyon/go-kata/module4/webserver/http/homework/models"

type UserService interface {
	Users() []models.User
	User(id int) (models.User, error)
	Add(user models.User) models.User
}

type userRepo struct {
	repo UserService
}

func New(r UserService) *userRepo {
	return &userRepo{
		repo: r,
	}
}

func (r *userRepo) Users() []models.User {
	return r.repo.Users()
}

func (r *userRepo) User(id int) (models.User, error) {
	return r.repo.User(id)
}

func (r *userRepo) Add(user models.User) models.User {
	return r.repo.Add(user)
}
