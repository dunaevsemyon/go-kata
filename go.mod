module gitlab.com/dunaevsemyon/go-kata

go 1.20

require gitlab.com/dunaevsemyon/greet v0.0.0-20230207145415-8c3315b799f0

require (
	github.com/AlecAivazis/survey/v2 v2.3.6
	github.com/brianvoe/gofakeit/v6 v6.21.0
	github.com/essentialkaos/translit/v2 v2.0.4
	github.com/go-chi/chi v1.5.4
	github.com/go-chi/chi/v5 v5.0.8
	github.com/go-chi/render v1.0.2
	github.com/json-iterator/go v1.1.12
	github.com/mattn/go-sqlite3 v1.14.16
	github.com/sirupsen/logrus v1.9.2
	golang.org/x/sync v0.2.0
)

require (
	github.com/ajg/form v1.5.1 // indirect
	github.com/kballard/go-shellquote v0.0.0-20180428030007-95032a82bc51 // indirect
	github.com/kr/pretty v0.3.1 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	github.com/mgutz/ansi v0.0.0-20170206155736-9520e82c474b // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/rogpeppe/go-internal v1.10.0 // indirect
	github.com/stretchr/testify v1.8.3 // indirect
	golang.org/x/sys v0.8.0 // indirect
	golang.org/x/term v0.8.0 // indirect
	golang.org/x/text v0.9.0 // indirect
)
